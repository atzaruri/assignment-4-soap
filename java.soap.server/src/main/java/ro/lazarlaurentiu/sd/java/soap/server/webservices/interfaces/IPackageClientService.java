package ro.lazarlaurentiu.sd.java.soap.server.webservices.interfaces;

import java.util.List;

import javax.jws.WebMethod;
import javax.jws.WebService;

import ro.lazarlaurentiu.sd.java.soap.server.models.Package;

@WebService
public interface IPackageClientService {

	@WebMethod
	public List<Package> getPackagesForUser(String username);

}
