$(document).ready(function() {
	var packageId = getUrlParameter('packageId');
	
	$.ajax({
		type: "GET",
		url: "/java.soap.client/client/packages/trackings?packageId=" + packageId,
		dataType: "json",
		
		success: function(data, textStatus, jqXHR) {
			$("#loggedInUser").html("Logged in user: " + data.loggedInUser);
			
			var trackings = $.parseJSON(data.trackings);
			
			$.each(trackings, function(i, item) {
				var row = $("<tr />")
				$('#trackings').append(row);
				row.append($("<td>" + item.trackingId + "</td>"));
				row.append($("<td>" + item.trackingCity + "</td>"));
				
				var timeStamp = item.trackingTimestamp.day + '-' +
							    item.trackingTimestamp.month + '-' +
							    item.trackingTimestamp.year + ' ' +
							    item.trackingTimestamp.hour + ':' +
							    item.trackingTimestamp.minute;
				if (item.trackingTimestamp.minute == 0) timeStamp += '0';
				
				row.append($("<td>" + timeStamp + "</td>"));
			});
		},
		
		error: function(jqXHR, textStatus, errorThrown) {
			$("#loggedInUser").html("An error occurred during get request. " + errorThrown);
		}
	});
});

function getUrlParameter(sParam) {
    var sPageURL = decodeURIComponent(window.location.search.substring(1)),
        sURLVariables = sPageURL.split('&'),
        sParameterName,
        i;

    for (i = 0; i < sURLVariables.length; i++) {
        sParameterName = sURLVariables[i].split('=');

        if (sParameterName[0] === sParam) {
            return sParameterName[1] === undefined ? true : sParameterName[1];
        }
    }
};