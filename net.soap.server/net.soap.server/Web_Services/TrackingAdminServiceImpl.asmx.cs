﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;

namespace net.soap.server.Web_Services
{

    [WebService(Namespace = "http://localhost/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]

    public class TrackingAdminServiceImpl : System.Web.Services.WebService
    {

        private Entities entities;

        public TrackingAdminServiceImpl()
        {
            entities = new Entities();
            entities.Configuration.ProxyCreationEnabled = false;
            entities.Configuration.LazyLoadingEnabled = false;
        }

        [WebMethod]
        public List<tracking> getTrackingsForPackage(int packageId)
        {
            if (validUserCredentials())
            {
               return entities.trackings.Where(t => t.packageId == packageId).ToList();
            }
            return null;
        }

        [WebMethod]
        public Boolean createTracking(int packageId, String trackingCity, DateTime trackingTimeStamp)
        {
            if (validUserCredentials())
            {
                tracking newTracking = entities.trackings.Create();
                newTracking.packageId = packageId;
                newTracking.trackingCity = trackingCity;
                newTracking.trackingTimeStamp = trackingTimeStamp;

                try
                {
                    entities.trackings.Add(newTracking);
                    entities.SaveChanges();
                    return true;
                }
                catch (Exception e) { }
            }
            return false;
        }

        [WebMethod]
        public Boolean deleteTracking(int trackingId)
        {
            if (validUserCredentials())
            {
                try
                {
                    tracking tracking = entities.trackings.Find(trackingId);
                    entities.trackings.Remove(tracking);
                    entities.SaveChanges();
                    return true;
                }
                catch (Exception e) { }
            }
            return false;
        }

        private Boolean validUserCredentials()
        {
            String username = this.Context.Request.Headers.Get("Username");
            String password = this.Context.Request.Headers.Get("Password");

            if (username != null && password != null)
            {
                user user = entities.users.Find(username);
                if (user != null && user.password == password && user.role == 1)
                {
                    return true;
                }
            }
            return false;
        }

    }
}
