
package ro.lazarlaurentiu.sd.net.soap.server;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="getTrackingsForPackageResult" type="{http://localhost/}ArrayOfTracking" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "getTrackingsForPackageResult"
})
@XmlRootElement(name = "getTrackingsForPackageResponse")
public class GetTrackingsForPackageResponse {

    protected ArrayOfTracking getTrackingsForPackageResult;

    /**
     * Gets the value of the getTrackingsForPackageResult property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfTracking }
     *     
     */
    public ArrayOfTracking getGetTrackingsForPackageResult() {
        return getTrackingsForPackageResult;
    }

    /**
     * Sets the value of the getTrackingsForPackageResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfTracking }
     *     
     */
    public void setGetTrackingsForPackageResult(ArrayOfTracking value) {
        this.getTrackingsForPackageResult = value;
    }

}
