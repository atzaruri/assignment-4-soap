package ro.lazarlaurentiu.sd.java.soap.client.servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.ws.BindingProvider;
import javax.xml.ws.handler.MessageContext;

import com.google.gson.Gson;
import com.google.gson.JsonObject;

import ro.lazarlaurentiu.sd.java.soap.server.webservices.interfaces.User;
import ro.lazarlaurentiu.sd.java.soap.server.webservices.interfaces.UserRole;
import ro.lazarlaurentiu.sd.net.soap.server.ArrayOfPackage;
import ro.lazarlaurentiu.sd.net.soap.server.PackageAdminServiceImpl;
import ro.lazarlaurentiu.sd.net.soap.server.PackageAdminServiceImplSoap;

/**
 * Servlet implementation class AdminPackagesServlet
 */
public class AdminPackagesServlet extends HttpServlet {

	private PackageAdminServiceImplSoap packageAdminService;

	private static final String CREATE_REQUEST = "/java.soap.client/admin/packages/create";
	private static final String TRACKING_REQUEST = "/java.soap.client/admin/packages/track";
	private static final String DELETE_REQUEST = "/java.soap.client/admin/packages/delete";

	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public AdminPackagesServlet() {
		super();
		packageAdminService = new PackageAdminServiceImpl().getPackageAdminServiceImplSoap();
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		User loggedInUser = (User) request.getSession().getAttribute("loggedInUser");

		if (loggedInUser != null && loggedInUser.getUserRole() == UserRole.ADMIN) {
			String url = request.getRequestURI();

			if (url.startsWith(CREATE_REQUEST)) {
				RequestDispatcher requestDispatcher = request
						.getRequestDispatcher("/admin/packages/createPackage.html");
				requestDispatcher.forward(request, response);
				return;
			}

			// Put Authentication Header
			addAuthenticationHeaders(loggedInUser);

			ArrayOfPackage packages = packageAdminService.getAllPackages();

			Gson gson = new Gson();
			JsonObject responseMessage = new JsonObject();
			PrintWriter responseWriter = response.getWriter();
			response.setContentType("text/hmtl");

			responseMessage.addProperty("loggedInUser", loggedInUser.getName());
			responseMessage.addProperty("packages", gson.toJson(packages.getPackage()));

			responseWriter.println(responseMessage.toString());
			responseWriter.close();
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		User loggedInUser = (User) request.getSession().getAttribute("loggedInUser");

		if (loggedInUser != null && loggedInUser.getUserRole() == UserRole.ADMIN) {
			String url = request.getRequestURI();
			Boolean operationResult = false;

			JsonObject responseMessage = new JsonObject();
			PrintWriter responseWriter = response.getWriter();
			response.setContentType("text/hmtl");

			if (url.startsWith(CREATE_REQUEST)) {
				String sender = (String) request.getParameter("sender");
				String receiver = (String) request.getParameter("receiver");
				String packageName = (String) request.getParameter("packageName");
				String packageDescription = (String) request.getParameter("packageDescription");
				String senderCity = (String) request.getParameter("senderCity");
				String destinationCity = (String) request.getParameter("destinationCity");

				// Add Authentication Headers
				addAuthenticationHeaders(loggedInUser);
				operationResult = packageAdminService.createPackage(sender, receiver, packageName, packageDescription,
						senderCity, destinationCity);
			}

			if (url.startsWith(TRACKING_REQUEST)) {
				String packageIdParameter = (String) request.getParameter("packageId");
				Integer packageId = null;
				try {
					packageId = Integer.parseInt(packageIdParameter);
				} catch (NumberFormatException e) {
					// Do nothing - Go Further
				}

				if (packageId != null) {
					// Add Authentication Headers
					addAuthenticationHeaders(loggedInUser);
					operationResult = packageAdminService.startPackageTracking(packageId);
					responseMessage.addProperty("message", "Package no. " + packageId + " is not tracked!");
				}
			}

			if (url.startsWith(DELETE_REQUEST)) {
				String packageIdParameter = (String) request.getParameter("packageId");
				Integer packageId = null;
				try {
					packageId = Integer.parseInt(packageIdParameter);
				} catch (NumberFormatException e) {
					// Do nothing - Go Further
				}

				if (packageId != null) {
					// Add Authentication Headers
					addAuthenticationHeaders(loggedInUser);
					operationResult = packageAdminService.deletePackage(packageId);
				}
			}

			responseMessage.addProperty("success", operationResult);

			responseWriter.println(responseMessage.toString());
			responseWriter.close();
		}
	}

	private void addAuthenticationHeaders(User loggedInUser) {
		Map<String, Object> requestContext = ((BindingProvider) packageAdminService).getRequestContext();
		Map<String, List<String>> requestHeaders = new HashMap<String, List<String>>();
		requestHeaders.put("Username", Collections.singletonList(loggedInUser.getUsername()));
		requestHeaders.put("Password", Collections.singletonList(loggedInUser.getPassword()));
		requestContext.put(MessageContext.HTTP_REQUEST_HEADERS, requestHeaders);
	}

}
