$(document).ready(function() {
	$.ajax({
		type: "GET",
		url: "/java.soap.client/client/packages",
		dataType: "json",
		
		success: function(data, textStatus, jqXHR) {
			$("#loggedInUser").html("Logged in user: " + data.loggedInUser);
			
			var packages = $.parseJSON(data.packages);
			
			$.each(packages, function(i, item) {
				var row = $("<tr />")
				$('#t01').append(row);
				row.append($("<td>" + item.packageId + "</td>"));
				row.append($("<td>" + item.sender.name + "</td>"));
				row.append($("<td>" + item.receiver.name + "</td>"));
				row.append($("<td>" + item.packageName + "</td>"));
				row.append($("<td>" + item.packageDescription + "</td>"));
				row.append($("<td>" + item.senderCity + "</td>"));
				row.append($("<td>" + item.destinationCity + "</td>"));
				if (item.packageTracking) {
					row.append($("<td><a href=/java.soap.client/client/packages/trackings.html?packageId=" + item.packageId + ">Tracked</a></td>"));
				} else {
					row.append($("<td>Not tracked</td>"));
				}
			});
		},
		
		error: function(jqXHR, textStatus, errorThrown) {
			$("#loggedInUser").html("An error occurred during get request. " + errorThrown);
		}
	});
});