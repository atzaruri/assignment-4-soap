$(document).ready(function() {
	var packageId = getUrlParameter('packageId');
	
	$('#packageId').val("" + packageId);
	
	$.ajax({
		type: "GET",
		url: "/java.soap.client/admin/packages/trackings?packageId=" + packageId,
		dataType: "json",
		
		success: function(data, textStatus, jqXHR) {
			$("#loggedInUser").html("Logged in user: " + data.loggedInUser + " (A)");
			
			var trackings = $.parseJSON(data.trackings);
			
			$.each(trackings, function(i, item) {
				var row = $("<tr />")
				$('#trackings').append(row);
				row.append($("<td>" + item.trackingId + "</td>"));
				row.append($("<td>" + item.trackingCity + "</td>"));
				
				var timeStamp = item.trackingTimeStamp.day + '-' +
							    item.trackingTimeStamp.month + '-' +
							    item.trackingTimeStamp.year + ' ' +
							    item.trackingTimeStamp.hour + ':' +
							    item.trackingTimeStamp.minute;
				if (item.trackingTimeStamp.minute == 0) timeStamp += '0';
				
				row.append($("<td>" + timeStamp + "</td>"));
				row.append($("<td><a href='#' onClick='deleteTracking(" + item.trackingId + ")'>Delete</a></td>"));
			});
		},
		
		error: function(jqXHR, textStatus, errorThrown) {
			$("#loggedInUser").html("An error occurred during get request. " + errorThrown);
		}
	});
});

function getUrlParameter(sParam) {
    var sPageURL = decodeURIComponent(window.location.search.substring(1)),
        sURLVariables = sPageURL.split('&'),
        sParameterName,
        i;

    for (i = 0; i < sURLVariables.length; i++) {
        sParameterName = sURLVariables[i].split('=');

        if (sParameterName[0] === sParam) {
            return sParameterName[1] === undefined ? true : sParameterName[1];
        }
    }
};

function deleteTracking(trackingId) {
	$.ajax({
		type: "POST",
		url: "/java.soap.client/admin/packages/trackings/delete",
		data: { 'trackingId' : trackingId },
		dataType: "json",
		
		success: function(data, textStatus, jqXHR) {
			if (data.success) {
				window.location.reload();
			} else {
				$('#message').html("An error occurred during the delete operation!");
			}
		},
		
		error: function(jqXHR, textStatus, errorThrown) {
			$("#message").html("An error occurred during post request. " + errorThrown);
		}
	});
};