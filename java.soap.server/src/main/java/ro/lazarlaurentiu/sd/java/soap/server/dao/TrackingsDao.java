package ro.lazarlaurentiu.sd.java.soap.server.dao;

import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import ro.lazarlaurentiu.sd.java.soap.server.models.Package;
import ro.lazarlaurentiu.sd.java.soap.server.models.Tracking;

/**
 * Data Access Object for Trackings.
 * 
 * @author Lazar Laurentiu
 */
public class TrackingsDao {

	private static final Log LOGGER = LogFactory.getLog(TrackingsDao.class);

	private SessionFactory factory;

	public TrackingsDao(SessionFactory factory) {
		this.factory = factory;
	}

	@SuppressWarnings("unchecked")
	public List<Tracking> getTrackingsForPackage(Package trackedPackage) {
		Session session = factory.openSession();
		Transaction transaction = null;
		List<Tracking> trackings = null;

		try {
			transaction = session.beginTransaction();
			Query query = session.createQuery("FROM Tracking WHERE packageId = :packageId");
			query.setParameter("packageId", trackedPackage);
			trackings = query.list();
			transaction.commit();
		} catch (HibernateException e) {
			if (transaction != null) {
				transaction.rollback();
			}
			LOGGER.error("Method: getTrackingsForPackage()", e);
		} finally {
			session.close();
		}
		return trackings;
	}

}