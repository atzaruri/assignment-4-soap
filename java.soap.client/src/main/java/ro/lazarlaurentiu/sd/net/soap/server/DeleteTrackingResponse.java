
package ro.lazarlaurentiu.sd.net.soap.server;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="deleteTrackingResult" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "deleteTrackingResult"
})
@XmlRootElement(name = "deleteTrackingResponse")
public class DeleteTrackingResponse {

    protected boolean deleteTrackingResult;

    /**
     * Gets the value of the deleteTrackingResult property.
     * 
     */
    public boolean isDeleteTrackingResult() {
        return deleteTrackingResult;
    }

    /**
     * Sets the value of the deleteTrackingResult property.
     * 
     */
    public void setDeleteTrackingResult(boolean value) {
        this.deleteTrackingResult = value;
    }

}
