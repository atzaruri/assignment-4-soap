package ro.lazarlaurentiu.sd.java.soap.server.webservices.publisher;

import javax.xml.ws.Endpoint;

import ro.lazarlaurentiu.sd.java.soap.server.webservices.implementations.PackageClientServiceImpl;
import ro.lazarlaurentiu.sd.java.soap.server.webservices.implementations.TrackingClientServiceImpl;
import ro.lazarlaurentiu.sd.java.soap.server.webservices.implementations.UserServiceImpl;

public class Publisher {

	private static final String BASE_ADDRESS = "http://localhost:8080";

	public static void main(String[] args) {
		Endpoint.publish(BASE_ADDRESS + "/users", new UserServiceImpl());
		Endpoint.publish(BASE_ADDRESS + "/client/packages", new PackageClientServiceImpl());
		Endpoint.publish(BASE_ADDRESS + "/client/packages/trackings", new TrackingClientServiceImpl());
	}

}
