
package ro.lazarlaurentiu.sd.net.soap.server;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="createPackageResult" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "createPackageResult"
})
@XmlRootElement(name = "createPackageResponse")
public class CreatePackageResponse {

    protected boolean createPackageResult;

    /**
     * Gets the value of the createPackageResult property.
     * 
     */
    public boolean isCreatePackageResult() {
        return createPackageResult;
    }

    /**
     * Sets the value of the createPackageResult property.
     * 
     */
    public void setCreatePackageResult(boolean value) {
        this.createPackageResult = value;
    }

}
