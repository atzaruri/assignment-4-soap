package ro.lazarlaurentiu.sd.java.soap.server.webservices.interfaces;

import javax.jws.WebMethod;
import javax.jws.WebService;

import ro.lazarlaurentiu.sd.java.soap.server.models.User;

@WebService
public interface IUserService {

	@WebMethod
	public boolean createUser(String username, String password, String name);

	@WebMethod
	public User getUser(String username, String password);

}
