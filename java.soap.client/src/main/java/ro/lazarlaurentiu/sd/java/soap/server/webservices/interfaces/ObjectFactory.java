
package ro.lazarlaurentiu.sd.java.soap.server.webservices.interfaces;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the ro.lazarlaurentiu.sd.java.soap.server.webservices.interfaces package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _GetTrackingsForPackageResponse_QNAME = new QName("http://interfaces.webservices.server.soap.java.sd.lazarlaurentiu.ro/", "getTrackingsForPackageResponse");
    private final static QName _GetTrackingsForPackage_QNAME = new QName("http://interfaces.webservices.server.soap.java.sd.lazarlaurentiu.ro/", "getTrackingsForPackage");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: ro.lazarlaurentiu.sd.java.soap.server.webservices.interfaces
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link GetTrackingsForPackage }
     * 
     */
    public GetTrackingsForPackage createGetTrackingsForPackage() {
        return new GetTrackingsForPackage();
    }

    /**
     * Create an instance of {@link GetTrackingsForPackageResponse }
     * 
     */
    public GetTrackingsForPackageResponse createGetTrackingsForPackageResponse() {
        return new GetTrackingsForPackageResponse();
    }

    /**
     * Create an instance of {@link Package }
     * 
     */
    public Package createPackage() {
        return new Package();
    }

    /**
     * Create an instance of {@link Tracking }
     * 
     */
    public Tracking createTracking() {
        return new Tracking();
    }

    /**
     * Create an instance of {@link User }
     * 
     */
    public User createUser() {
        return new User();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetTrackingsForPackageResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://interfaces.webservices.server.soap.java.sd.lazarlaurentiu.ro/", name = "getTrackingsForPackageResponse")
    public JAXBElement<GetTrackingsForPackageResponse> createGetTrackingsForPackageResponse(GetTrackingsForPackageResponse value) {
        return new JAXBElement<GetTrackingsForPackageResponse>(_GetTrackingsForPackageResponse_QNAME, GetTrackingsForPackageResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetTrackingsForPackage }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://interfaces.webservices.server.soap.java.sd.lazarlaurentiu.ro/", name = "getTrackingsForPackage")
    public JAXBElement<GetTrackingsForPackage> createGetTrackingsForPackage(GetTrackingsForPackage value) {
        return new JAXBElement<GetTrackingsForPackage>(_GetTrackingsForPackage_QNAME, GetTrackingsForPackage.class, null, value);
    }

}
