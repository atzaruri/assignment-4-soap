package ro.lazarlaurentiu.sd.java.soap.client.servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.ws.BindingProvider;
import javax.xml.ws.handler.MessageContext;

import com.google.gson.Gson;
import com.google.gson.JsonObject;

import ro.lazarlaurentiu.sd.java.soap.server.webservices.implementations.IPackageClientService;
import ro.lazarlaurentiu.sd.java.soap.server.webservices.implementations.PackageClientServiceImplService;
import ro.lazarlaurentiu.sd.java.soap.server.webservices.interfaces.Package;
import ro.lazarlaurentiu.sd.java.soap.server.webservices.interfaces.User;

/**
 * Servlet implementation class ClientPackagesServlet
 */
public class ClientPackagesServlet extends HttpServlet {

	private IPackageClientService packageClientService;

	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public ClientPackagesServlet() {
		super();
		packageClientService = new PackageClientServiceImplService().getPackageClientServiceImplPort();
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		User loggedInUser = (User) request.getSession().getAttribute("loggedInUser");

		if (loggedInUser != null) {
			// Put Authentication Header
			Map<String, Object> requestContext = ((BindingProvider) packageClientService).getRequestContext();
			Map<String, List<String>> requestHeaders = new HashMap<String, List<String>>();
			requestHeaders.put("Username", Collections.singletonList(loggedInUser.getUsername()));
			requestHeaders.put("Password", Collections.singletonList(loggedInUser.getPassword()));
			requestContext.put(MessageContext.HTTP_REQUEST_HEADERS, requestHeaders);

			List<Package> packages = packageClientService.getPackagesForUser(loggedInUser.getUsername());

			Gson gson = new Gson();
			JsonObject responseMessage = new JsonObject();
			PrintWriter responseWriter = response.getWriter();
			response.setContentType("text/hmtl");

			responseMessage.addProperty("loggedInUser", loggedInUser.getName());
			responseMessage.addProperty("packages", gson.toJson(packages));

			responseWriter.println(responseMessage.toString());
			responseWriter.close();
		} else {
			RequestDispatcher requestDispatcher = request.getRequestDispatcher("/login");
			requestDispatcher.forward(request, response);
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doGet(request, response);
	}

}
