package ro.lazarlaurentiu.sd.java.soap.server.webservices.implementations;

import java.util.List;

import javax.annotation.Resource;
import javax.jws.WebService;
import javax.xml.ws.WebServiceContext;

import org.hibernate.cfg.Configuration;

import ro.lazarlaurentiu.sd.java.soap.server.dao.PackagesDao;
import ro.lazarlaurentiu.sd.java.soap.server.models.Package;
import ro.lazarlaurentiu.sd.java.soap.server.models.User;
import ro.lazarlaurentiu.sd.java.soap.server.webservices.interfaces.IPackageClientService;

@WebService(endpointInterface = "ro.lazarlaurentiu.sd.java.soap.server.webservices.interfaces.IPackageClientService")
public class PackageClientServiceImpl implements IPackageClientService {

	@Resource
	private WebServiceContext webServiceContext;

	private UserServiceImpl userService;
	private PackagesDao packagesDao;

	public PackageClientServiceImpl() {
		userService = new UserServiceImpl();
		packagesDao = new PackagesDao(new Configuration().configure().buildSessionFactory());
	}

	public List<Package> getPackagesForUser(String username) {
		User user = userService.verifyUserCredentials(webServiceContext);

		if (user != null && user.getUsername().equals(username)) {
			return packagesDao.getPackagesForUser(user);
		}

		return null;
	}

}
