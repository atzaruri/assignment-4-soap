package ro.lazarlaurentiu.sd.java.soap.client.servlets;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.codec.digest.DigestUtils;

import com.google.gson.JsonObject;

import ro.lazarlaurentiu.sd.java.soap.server.webservices.implementations.IUserService;
import ro.lazarlaurentiu.sd.java.soap.server.webservices.implementations.UserServiceImplService;
import ro.lazarlaurentiu.sd.java.soap.server.webservices.interfaces.User;
import ro.lazarlaurentiu.sd.java.soap.server.webservices.interfaces.UserRole;

/**
 * Servlet implementation class LoginServlet
 */
public class LoginServlet extends HttpServlet {

	private IUserService userService;

	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public LoginServlet() {
		super();
		userService = new UserServiceImplService().getUserServiceImplPort();
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		RequestDispatcher requestDispatcher = request.getRequestDispatcher("login.html");
		requestDispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		String username = request.getParameter("username");
		String password = DigestUtils.md5Hex(request.getParameter("password"));

		User user = userService.getUser(username, password);

		JsonObject responseMessage = new JsonObject();
		PrintWriter responseWriter = response.getWriter();
		response.setContentType("text/html");

		if (user == null || user.getUserRole() == UserRole.INVALID_USER) {
			responseMessage.addProperty("success", false);
			responseMessage.addProperty("message", "Invalid username or password!");
		} else {
			responseMessage.addProperty("success", true);
			responseMessage.addProperty("role", user.getUserRole().toString());
			request.getSession().setAttribute("loggedInUser", user);
		}

		responseWriter.println(responseMessage.toString());
		responseWriter.close();

		return;
	}

}
