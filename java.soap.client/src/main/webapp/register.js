$(document).ready(function() {
	
	$("#registerForm").submit(function(e) {
		e.preventDefault();
	});
	
	$("#registerButton").click(function(e) {
		dataString = $("#registerForm").serialize();
		
		$.ajax({
			type: "POST",
			url: "/java.soap.client/register",
			data: dataString,
			dataType: "json",
			
			success: function(data, textStatus, jqXHR) {
				$("#message").html(data.message);
				
				// Redirect to Login Page
				setTimeout(function() {
					window.location.href = "/java.soap.client/login"
				}, 750);
			},
			
			error: function(jqXHR, textStatus, errorThrown) {
				$("#message").html("An error occurred during post request. " + errorThrown);
			}
		});
	});
	
});