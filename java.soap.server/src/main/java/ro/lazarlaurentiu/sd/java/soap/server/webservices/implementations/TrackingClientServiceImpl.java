package ro.lazarlaurentiu.sd.java.soap.server.webservices.implementations;

import java.util.List;

import javax.annotation.Resource;
import javax.jws.WebService;
import javax.xml.ws.WebServiceContext;

import org.hibernate.cfg.Configuration;

import ro.lazarlaurentiu.sd.java.soap.server.dao.PackagesDao;
import ro.lazarlaurentiu.sd.java.soap.server.dao.TrackingsDao;
import ro.lazarlaurentiu.sd.java.soap.server.models.Package;
import ro.lazarlaurentiu.sd.java.soap.server.models.Tracking;
import ro.lazarlaurentiu.sd.java.soap.server.models.User;
import ro.lazarlaurentiu.sd.java.soap.server.webservices.interfaces.ITrackingClientService;

@WebService(endpointInterface = "ro.lazarlaurentiu.sd.java.soap.server.webservices.interfaces.ITrackingClientService")
public class TrackingClientServiceImpl implements ITrackingClientService {

	@Resource
	private WebServiceContext webServiceContext;

	private UserServiceImpl userService;
	private PackagesDao packagesDao;
	private TrackingsDao trackingsDao;

	public TrackingClientServiceImpl() {
		userService = new UserServiceImpl();
		packagesDao = new PackagesDao(new Configuration().configure().buildSessionFactory());
		trackingsDao = new TrackingsDao(new Configuration().configure().buildSessionFactory());
	}

	public List<Tracking> getTrackingsForPackage(int packageId) {
		User user = userService.verifyUserCredentials(webServiceContext);

		if (user != null) {
			Package trackedPackage = packagesDao.getPackagesById(packageId);
			if (trackedPackage != null && (trackedPackage.getSender().getUsername().equals(user.getUsername())
					|| trackedPackage.getReceiver().getUsername().equals(user.getUsername()))) {
				return trackingsDao.getTrackingsForPackage(trackedPackage);
			}
		}

		return null;
	}

}
