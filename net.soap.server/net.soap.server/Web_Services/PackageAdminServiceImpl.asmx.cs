﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.Services.Protocols;

namespace net.soap.server.Web_Services
{
    [WebService(Namespace = "http://localhost/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]

    public class PackageAdminServiceImpl : System.Web.Services.WebService
    {
        private Entities entities;

        public PackageAdminServiceImpl()
        {
            entities = new Entities();
            entities.Configuration.ProxyCreationEnabled = false;
            entities.Configuration.LazyLoadingEnabled = false;
        }

        [WebMethod]
        public List<package> getAllPackages()
        {
            if (validUserCredentials())
            {
                return entities.packages.ToList();
            }
            return null;
        }

        [WebMethod]
        public Boolean createPackage(String sender, String receiver, String packageName, String packageDescription, 
            String senderCity, String destinationCity)
        {
            if (validUserCredentials())
            {
                package newPackage = entities.packages.Create();
                newPackage.sender = sender;
                newPackage.receiver = receiver;
                newPackage.packageName = packageName;
                newPackage.packageDescription = packageDescription;
                newPackage.senderCity = senderCity;
                newPackage.destinationCity = destinationCity;

                try
                {
                    entities.packages.Add(newPackage);
                    entities.SaveChanges();
                    return true;
                }
                catch (Exception e) { }
            }
            return false;
        }

        [WebMethod]
        public Boolean deletePackage(int packageId)
        {
            if (validUserCredentials())
            {
                try
                {
                    package package = entities.packages.Find(packageId);
                    entities.packages.Remove(package);
                    entities.SaveChanges();
                    return true;
                }
                catch (Exception e) { }
            }
            return false;
        }

        [WebMethod]
        public Boolean startPackageTracking(int packageId)
        {
            if (validUserCredentials())
            {   
                try
                {
                    package package = entities.packages.Find(packageId);
                    package.packageTracking = 1;
                    entities.SaveChanges();
                    return true;
                }
                catch (Exception e) { }
            }
            return false;
        }

        private Boolean validUserCredentials()
        {
            String username = this.Context.Request.Headers.Get("Username");
            String password = this.Context.Request.Headers.Get("Password");

            if (username != null && password != null)
            {
                user user = entities.users.Find(username);
                if (user != null && user.password == password && user.role == 1)
                {
                    return true;
                }
            }
            return false;
        }
    }
}
