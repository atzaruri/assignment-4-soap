package ro.lazarlaurentiu.sd.java.soap.server.webservices.interfaces;

import java.util.List;

import javax.jws.WebMethod;
import javax.jws.WebService;

import ro.lazarlaurentiu.sd.java.soap.server.models.Tracking;

@WebService
public interface ITrackingClientService {

	@WebMethod
	public List<Tracking> getTrackingsForPackage(int packageId);

}
