package ro.lazarlaurentiu.sd.java.soap.client.servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import javax.xml.ws.BindingProvider;
import javax.xml.ws.handler.MessageContext;

import com.google.gson.Gson;
import com.google.gson.JsonObject;

import ro.lazarlaurentiu.sd.java.soap.server.webservices.interfaces.User;
import ro.lazarlaurentiu.sd.java.soap.server.webservices.interfaces.UserRole;
import ro.lazarlaurentiu.sd.net.soap.server.ArrayOfTracking;
import ro.lazarlaurentiu.sd.net.soap.server.TrackingAdminServiceImpl;
import ro.lazarlaurentiu.sd.net.soap.server.TrackingAdminServiceImplSoap;

/**
 * Servlet implementation class TrackingAdminServlet
 */
public class AdminTrackingServlet extends HttpServlet {

	private TrackingAdminServiceImplSoap trackingAdminService;

	private static final SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("dd-MM-yyyy HH:mm");
	private static final String CREATE_REQUEST = "/java.soap.client/admin/packages/trackings/create";
	private static final String DELETE_REQUEST = "/java.soap.client/admin/packages/trackings/delete";

	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public AdminTrackingServlet() {
		super();
		trackingAdminService = new TrackingAdminServiceImpl().getTrackingAdminServiceImplSoap();
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		User loggedInUser = (User) request.getSession().getAttribute("loggedInUser");

		if (loggedInUser != null && loggedInUser.getUserRole() == UserRole.ADMIN) {
			String url = request.getRequestURI();

			if (url.startsWith(CREATE_REQUEST)) {
				RequestDispatcher requestDispatcher = request
						.getRequestDispatcher("/admin/packages/trackings/createTracking.html");
				requestDispatcher.forward(request, response);
				return;
			}

			// Put Authentication Headers
			addAuthenticationHeaders(loggedInUser);

			String packageIdParameter = request.getParameter("packageId");
			Integer packageId = null;
			ArrayOfTracking trackings = null;
			try {
				packageId = Integer.parseInt(packageIdParameter);
				trackings = trackingAdminService.getTrackingsForPackage(packageId);
			} catch (NumberFormatException e) {
				// No nothing - Go further
			}

			Gson gson = new Gson();
			JsonObject responseMessage = new JsonObject();
			PrintWriter responseWriter = response.getWriter();
			response.setContentType("text/hmtl");

			responseMessage.addProperty("loggedInUser", loggedInUser.getName());
			responseMessage.addProperty("trackings", gson.toJson(trackings.getTracking()));

			responseWriter.println(responseMessage.toString());
			responseWriter.close();
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		User loggedInUser = (User) request.getSession().getAttribute("loggedInUser");

		if (loggedInUser != null && loggedInUser.getUserRole() == UserRole.ADMIN) {
			String url = request.getRequestURI();
			Boolean operationResult = false;

			JsonObject responseMessage = new JsonObject();
			PrintWriter responseWriter = response.getWriter();
			response.setContentType("text/hmtl");

			if (url.startsWith(CREATE_REQUEST)) {
				String packageIdParameter = (String) request.getParameter("packageId");
				String trackingCity = (String) request.getParameter("trackingCity");
				XMLGregorianCalendar trackingTimestamp = null;
				try {
					int packageId = Integer.parseInt(packageIdParameter);
					Date trackingDate = DATE_FORMAT.parse((String) request.getParameter("trackingTimeStamp"));
					GregorianCalendar gregorianCalendar = new GregorianCalendar();
					gregorianCalendar.setTime(trackingDate);
					trackingTimestamp = DatatypeFactory.newInstance().newXMLGregorianCalendar(gregorianCalendar);

					// Add Authentication Headers
					addAuthenticationHeaders(loggedInUser);
					operationResult = trackingAdminService.createTracking(packageId, trackingCity, trackingTimestamp);
				} catch (Exception e) {
					// Do Nothing - Go Further
				}
			}

			if (url.startsWith(DELETE_REQUEST)) {
				String trackingIdAttribute = (String) request.getParameter("trackingId");
				Integer trackingId = null;
				try {
					trackingId = Integer.parseInt(trackingIdAttribute);
				} catch (NumberFormatException e) {
					// Do nothing - Go Further
				}

				if (trackingId != null) {
					// Add Authentication Headers
					addAuthenticationHeaders(loggedInUser);
					operationResult = trackingAdminService.deleteTracking(trackingId);
				}
			}

			responseMessage.addProperty("success", operationResult);

			responseWriter.println(responseMessage.toString());
			responseWriter.close();
		}
	}

	private void addAuthenticationHeaders(User loggedInUser) {
		Map<String, Object> requestContext = ((BindingProvider) trackingAdminService).getRequestContext();
		Map<String, List<String>> requestHeaders = new HashMap<String, List<String>>();
		requestHeaders.put("Username", Collections.singletonList(loggedInUser.getUsername()));
		requestHeaders.put("Password", Collections.singletonList(loggedInUser.getPassword()));
		requestContext.put(MessageContext.HTTP_REQUEST_HEADERS, requestHeaders);
	}

}
