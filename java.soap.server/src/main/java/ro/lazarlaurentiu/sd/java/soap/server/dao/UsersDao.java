package ro.lazarlaurentiu.sd.java.soap.server.dao;

import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import ro.lazarlaurentiu.sd.java.soap.server.models.User;

/**
 * Data Access Object for Users.
 * 
 * @author Lazar Laurentiu
 */
public class UsersDao {

	private static final Log LOGGER = LogFactory.getLog(UsersDao.class);

	private SessionFactory factory;

	public UsersDao(SessionFactory factory) {
		this.factory = factory;
	}

	public boolean createUser(User user) {
		Session session = factory.openSession();
		Transaction transaction = null;

		try {
			transaction = session.beginTransaction();
			session.save(user);
			transaction.commit();
		} catch (HibernateException e) {
			if (transaction != null) {
				transaction.rollback();
			}

			LOGGER.error("Method: createUser()", e);

			return false;
		} finally {
			session.close();
		}

		return true;
	}

	@SuppressWarnings("unchecked")
	public User getUser(String username) {
		Session session = factory.openSession();
		Transaction transaction = null;
		List<User> users = null;

		try {
			transaction = session.beginTransaction();
			Query query = session.createQuery("FROM User WHERE username = :username");
			query.setParameter("username", username);
			users = query.list();
			transaction.commit();
		} catch (HibernateException e) {
			if (transaction != null) {
				transaction.rollback();
			}
			LOGGER.error("Method: getUser()", e);
		} finally {
			session.close();
		}
		return users != null && !users.isEmpty() ? users.get(0) : null;
	}

}
