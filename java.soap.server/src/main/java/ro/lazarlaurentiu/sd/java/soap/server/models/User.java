package ro.lazarlaurentiu.sd.java.soap.server.models;

import java.io.Serializable;

/**
 * Model Class for an User.
 * 
 * @author Lazar Laurentiu
 */
public class User implements Serializable {

	public enum UserRole {
		INVALID_USER, ADMIN, REGULAR_USER
	};

	private String username;
	private String password;
	private String name;
	private UserRole userRole;

	private static final long serialVersionUID = 1L;

	public User() {
	}

	public User(String username, String password, String name, UserRole userRole) {
		super();
		this.username = username;
		this.password = password;
		this.name = name;
		this.userRole = userRole;
	}

	public String getUsername() {
		return this.username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public UserRole getUserRole() {
		return userRole;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setUserRole(UserRole userRole) {
		this.userRole = userRole;
	}

}
