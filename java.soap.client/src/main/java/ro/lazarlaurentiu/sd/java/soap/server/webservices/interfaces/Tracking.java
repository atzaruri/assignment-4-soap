
package ro.lazarlaurentiu.sd.java.soap.server.webservices.interfaces;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;

import com.google.gson.annotations.Expose;


/**
 * <p>Java class for tracking complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="tracking">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="trackedPackage" type="{http://interfaces.webservices.server.soap.java.sd.lazarlaurentiu.ro/}package" minOccurs="0"/>
 *         &lt;element name="trackingCity" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="trackingId" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="trackingTimestamp" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "tracking", propOrder = {
    "trackedPackage",
    "trackingCity",
    "trackingId",
    "trackingTimestamp"
})
public class Tracking {
	
	@Expose(serialize = false)
    protected Package trackedPackage;
    protected String trackingCity;
    protected int trackingId;
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar trackingTimestamp;

    /**
     * Gets the value of the trackedPackage property.
     * 
     * @return
     *     possible object is
     *     {@link Package }
     *     
     */
    public Package getTrackedPackage() {
        return trackedPackage;
    }

    /**
     * Sets the value of the trackedPackage property.
     * 
     * @param value
     *     allowed object is
     *     {@link Package }
     *     
     */
    public void setTrackedPackage(Package value) {
        this.trackedPackage = value;
    }

    /**
     * Gets the value of the trackingCity property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTrackingCity() {
        return trackingCity;
    }

    /**
     * Sets the value of the trackingCity property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTrackingCity(String value) {
        this.trackingCity = value;
    }

    /**
     * Gets the value of the trackingId property.
     * 
     */
    public int getTrackingId() {
        return trackingId;
    }

    /**
     * Sets the value of the trackingId property.
     * 
     */
    public void setTrackingId(int value) {
        this.trackingId = value;
    }

    /**
     * Gets the value of the trackingTimestamp property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getTrackingTimestamp() {
        return trackingTimestamp;
    }

    /**
     * Sets the value of the trackingTimestamp property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setTrackingTimestamp(XMLGregorianCalendar value) {
        this.trackingTimestamp = value;
    }

}
