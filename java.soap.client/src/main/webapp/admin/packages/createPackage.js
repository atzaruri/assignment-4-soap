$(document).ready(function() {
	
	$("#packageForm").submit(function(e) {
		e.preventDefault();
	});
	
	$("#createButton").click(function(e) {
		dataString = $("#packageForm").serialize();
		
		$.ajax({
			type: "POST",
			url: "/java.soap.client/admin/packages/create",
			data: dataString,
			dataType: "json",
			
			success: function(data, textStatus, jqXHR) {
				if (data.success) {
					window.location.href = "/java.soap.client/admin/packages.html"
				} else {
					$("#message").html("An error occurred during creation!");
				}
			},
			
			error: function(jqXHR, textStatus, errorThrown) {
				$("#message").html("An error occurred during post request. " + errorThrown);
			}
		});
	});
	
});