$(document).ready(function() {
	
	$("#loginForm").submit(function(e) {
		e.preventDefault();
	});
	
	$("#loginButton").click(function(e) {
		$("#message").html("");
		dataString = $("#loginForm").serialize();
		
		$.ajax({
			type: "POST",
			url: "/java.soap.client/login",
			data: dataString,
			dataType: "json",
			
			success: function(data, textStatus, jqXHR) {
				if (data.success) {
					if (data.role == 'ADMIN') {
						window.location.href = "/java.soap.client/admin/packages.html"
					} else {
						window.location.href = "/java.soap.client/client/packages.html"
					}
				} else {
					$("#message").html(data.message);
				}
			},
			
			error: function(jqXHR, textStatus, errorThrown) {
				$("#message").html("An error occurred during post request. " + errorThrown);
			}
		});
	});
	
});