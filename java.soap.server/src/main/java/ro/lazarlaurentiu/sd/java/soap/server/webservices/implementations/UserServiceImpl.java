package ro.lazarlaurentiu.sd.java.soap.server.webservices.implementations;

import java.util.List;
import java.util.Map;

import javax.jws.WebService;
import javax.xml.ws.WebServiceContext;
import javax.xml.ws.handler.MessageContext;

import org.hibernate.cfg.Configuration;

import ro.lazarlaurentiu.sd.java.soap.server.dao.UsersDao;
import ro.lazarlaurentiu.sd.java.soap.server.models.User;
import ro.lazarlaurentiu.sd.java.soap.server.models.User.UserRole;
import ro.lazarlaurentiu.sd.java.soap.server.webservices.interfaces.IUserService;

@WebService(endpointInterface = "ro.lazarlaurentiu.sd.java.soap.server.webservices.interfaces.IUserService")
public class UserServiceImpl implements IUserService {

	private UsersDao usersDao;

	public UserServiceImpl() {
		usersDao = new UsersDao(new Configuration().configure().buildSessionFactory());
	}

	public User getUser(String username, String password) {
		User user = usersDao.getUser(username);

		if (user == null || user.getPassword().compareTo(password) != 0) {
			return null;
		}

		return user;
	}

	public boolean createUser(String username, String password, String name) {
		User user = new User(username, password, name, UserRole.REGULAR_USER);
		return usersDao.createUser(user);
	}

	@SuppressWarnings("rawtypes")
	public User verifyUserCredentials(WebServiceContext webServiceContext) {
		MessageContext messageContext = webServiceContext.getMessageContext();
		Map httpHeaders = (Map) messageContext.get(MessageContext.HTTP_REQUEST_HEADERS);

		List userList = (List) httpHeaders.get("Username");
		List passList = (List) httpHeaders.get("Password");

		if (userList != null && passList != null) {
			String username = userList.get(0).toString();
			String password = passList.get(0).toString();

			User user = getUser(username, password);

			return user;
		}

		return null;
	}

}
