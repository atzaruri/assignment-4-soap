package ro.lazarlaurentiu.sd.java.soap.server.models;

import java.io.Serializable;

/**
 * Model Class for a Package.
 * 
 * @author Lazar Laurentiu
 */
public class Package implements Serializable {

	private int packageId;
	private User sender;
	private User receiver;
	private String packageName;
	private String packageDescription;
	private String senderCity;
	private String destinationCity;
	private boolean packageTracking;

	private static final long serialVersionUID = 1L;

	public Package() {
	}

	public int getPackageId() {
		return packageId;
	}

	public void setPackageId(int packageId) {
		this.packageId = packageId;
	}

	public User getSender() {
		return sender;
	}

	public void setSender(User sender) {
		this.sender = sender;
	}

	public User getReceiver() {
		return receiver;
	}

	public void setReceiver(User receiver) {
		this.receiver = receiver;
	}

	public String getPackageName() {
		return packageName;
	}

	public void setPackageName(String packageName) {
		this.packageName = packageName;
	}

	public String getPackageDescription() {
		return packageDescription;
	}

	public void setPackageDescription(String packageDescription) {
		this.packageDescription = packageDescription;
	}

	public String getSenderCity() {
		return senderCity;
	}

	public void setSenderCity(String senderCity) {
		this.senderCity = senderCity;
	}

	public String getDestinationCity() {
		return destinationCity;
	}

	public void setDestinationCity(String destinationCity) {
		this.destinationCity = destinationCity;
	}

	public boolean getPackageTracking() {
		return packageTracking;
	}

	public void setPackageTracking(boolean packageTracking) {
		this.packageTracking = packageTracking;
	}

}
