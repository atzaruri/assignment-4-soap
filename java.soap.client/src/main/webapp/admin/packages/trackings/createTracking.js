$(document).ready(function() {
	var packageId = getUrlParameter('packageId');
	$("#packageId").val("" + packageId);
	
	$("#trackingForm").submit(function(e) {
		e.preventDefault();
	});
	
	$("#createButton").click(function(e) {
		dataString = $("#trackingForm").serialize();
		
		$.ajax({
			type: "POST",
			url: "/java.soap.client/admin/packages/trackings/create",
			data: dataString,
			dataType: "json",
			
			success: function(data, textStatus, jqXHR) {
				if (data.success) {
					window.location.href = "/java.soap.client/admin/packages/trackings.html?packageId=" + packageId;
				} else {
					$("#message").html("An error occurred during creation!");
				}
			},
			
			error: function(jqXHR, textStatus, errorThrown) {
				$("#message").html("An error occurred during post request. " + errorThrown);
			}
		});
	});
	
});

function getUrlParameter(sParam) {
    var sPageURL = decodeURIComponent(window.location.search.substring(1)),
        sURLVariables = sPageURL.split('&'),
        sParameterName,
        i;

    for (i = 0; i < sURLVariables.length; i++) {
        sParameterName = sURLVariables[i].split('=');

        if (sParameterName[0] === sParam) {
            return sParameterName[1] === undefined ? true : sParameterName[1];
        }
    }
};