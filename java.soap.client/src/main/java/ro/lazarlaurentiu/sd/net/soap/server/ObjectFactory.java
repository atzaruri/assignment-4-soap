
package ro.lazarlaurentiu.sd.net.soap.server;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the localhost package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: localhost
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link StartPackageTracking }
     * 
     */
    public StartPackageTracking createStartPackageTracking() {
        return new StartPackageTracking();
    }

    /**
     * Create an instance of {@link CreatePackageResponse }
     * 
     */
    public CreatePackageResponse createCreatePackageResponse() {
        return new CreatePackageResponse();
    }

    /**
     * Create an instance of {@link DeletePackageResponse }
     * 
     */
    public DeletePackageResponse createDeletePackageResponse() {
        return new DeletePackageResponse();
    }

    /**
     * Create an instance of {@link CreatePackage }
     * 
     */
    public CreatePackage createCreatePackage() {
        return new CreatePackage();
    }

    /**
     * Create an instance of {@link StartPackageTrackingResponse }
     * 
     */
    public StartPackageTrackingResponse createStartPackageTrackingResponse() {
        return new StartPackageTrackingResponse();
    }

    /**
     * Create an instance of {@link GetAllPackagesResponse }
     * 
     */
    public GetAllPackagesResponse createGetAllPackagesResponse() {
        return new GetAllPackagesResponse();
    }

    /**
     * Create an instance of {@link ArrayOfPackage }
     * 
     */
    public ArrayOfPackage createArrayOfPackage() {
        return new ArrayOfPackage();
    }

    /**
     * Create an instance of {@link DeletePackage }
     * 
     */
    public DeletePackage createDeletePackage() {
        return new DeletePackage();
    }

    /**
     * Create an instance of {@link GetAllPackages }
     * 
     */
    public GetAllPackages createGetAllPackages() {
        return new GetAllPackages();
    }

    /**
     * Create an instance of {@link Package }
     * 
     */
    public Package createPackage() {
        return new Package();
    }

    /**
     * Create an instance of {@link User }
     * 
     */
    public User createUser() {
        return new User();
    }

}
