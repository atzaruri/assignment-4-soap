
package ro.lazarlaurentiu.sd.net.soap.server;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="createTrackingResult" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "createTrackingResult"
})
@XmlRootElement(name = "createTrackingResponse")
public class CreateTrackingResponse {

    protected boolean createTrackingResult;

    /**
     * Gets the value of the createTrackingResult property.
     * 
     */
    public boolean isCreateTrackingResult() {
        return createTrackingResult;
    }

    /**
     * Sets the value of the createTrackingResult property.
     * 
     */
    public void setCreateTrackingResult(boolean value) {
        this.createTrackingResult = value;
    }

}
