package ro.lazarlaurentiu.sd.java.soap.client.servlets;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.codec.digest.DigestUtils;

import com.google.gson.JsonObject;

import ro.lazarlaurentiu.sd.java.soap.server.webservices.implementations.IUserService;
import ro.lazarlaurentiu.sd.java.soap.server.webservices.implementations.UserServiceImplService;

/**
 * Servlet implementation class RegistrationServlet
 */
public class RegistrationServlet extends HttpServlet {

	private IUserService userService;

	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public RegistrationServlet() {
		super();
		userService = new UserServiceImplService().getUserServiceImplPort();
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		RequestDispatcher requestDispatcher = request.getRequestDispatcher("register.html");
		requestDispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String username = request.getParameter("username");
		String password = DigestUtils.md5Hex(request.getParameter("password"));
		String name = request.getParameter("name");

		JsonObject responseMessage = new JsonObject();
		PrintWriter responseWriter = response.getWriter();
		response.setContentType("text/hmtl");

		if (!userService.createUser(username, password, name)) {
			// Registration failed
			responseMessage.addProperty("success", false);
			responseMessage.addProperty("message", "Username already in use!");
		} else {
			responseMessage.addProperty("success", true);
			responseMessage.addProperty("message", "Registration successful!");
		}

		responseWriter.println(responseMessage.toString());
		responseWriter.close();
	}

}
