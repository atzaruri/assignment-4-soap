$(document).ready(function() {
	$.ajax({
		type: "GET",
		url: "/java.soap.client/admin/packages",
		dataType: "json",
		
		success: function(data, textStatus, jqXHR) {
			$("#loggedInUser").html("Logged in user: " + data.loggedInUser + " (A)");
			
			var packages = $.parseJSON(data.packages);
			
			$.each(packages, function(i, item) {
				var row = $("<tr />")
				$('#t01').append(row);
				row.append($("<td>" + item.packageId + "</td>"));
				row.append($("<td>" + item.sender + "</td>"));
				row.append($("<td>" + item.receiver + "</td>"));
				row.append($("<td>" + item.packageName + "</td>"));
				row.append($("<td>" + item.packageDescription + "</td>"));
				row.append($("<td>" + item.senderCity + "</td>"));
				row.append($("<td>" + item.destinationCity + "</td>"));
				if (item.packageTracking) {
					row.append($("<td><a href=/java.soap.client/admin/packages/trackings.html?packageId=" + item.packageId + ">Tracked</a></td>"));
				} else {
					row.append($("<td><a href='#' onClick='trackPackage(" + item.packageId + ")'>Not tracked. Start tracking.</a></td>"));
				}
				row.append($("<td><a href='#' onClick='deletePackage(" + item.packageId + ")'>Delete</a></td>"));
				
			});
		},
		
		error: function(jqXHR, textStatus, errorThrown) {
			$("#loggedInUser").html("An error occurred during get request. " + errorThrown);
		}
	});
});

function deletePackage(packageId) {
	$.ajax({
		type: "POST",
		url: "/java.soap.client/admin/packages/delete",
		data: { 'packageId' : packageId },
		dataType: "json",
		
		success: function(data, textStatus, jqXHR) {
			if (data.success) {
				window.location.href = "/java.soap.client/admin/packages.html"
			} else {
				$('#message').html("An error occurred during the delete operation!")
			}
		},
		
		error: function(jqXHR, textStatus, errorThrown) {
			$("#message").html("An error occurred during post request. " + errorThrown);
		}
	});
};

function trackPackage(packageId) {
	$.ajax({
		type: "POST",
		url: "/java.soap.client/admin/packages/track",
		data: { 'packageId' : packageId },
		dataType: "json",
		
		success: function(data, textStatus, jqXHR) {
			if (data.success) {
				window.location.reload();
			} else {
				$('#message').html("An error occurred during the start tracking operation!")
			}
		},
		
		error: function(jqXHR, textStatus, errorThrown) {
			$("#message").html("An error occurred during post request. " + errorThrown);
		}
	});
};