package ro.lazarlaurentiu.sd.java.soap.server.models;

import java.io.Serializable;
import java.util.Date;

public class Tracking implements Serializable {
	
	private int trackingId;
	private Package trackedPackage;
	private String trackingCity;
	private Date trackingTimestamp;

	private static final long serialVersionUID = 1L;
	
	public int getTrackingId() {
		return trackingId;
	}

	public void setTrackingId(int trackingId) {
		this.trackingId = trackingId;
	}

	public Package getTrackedPackage() {
		return trackedPackage;
	}

	public void setTrackedPackage(Package trackedPackage) {
		this.trackedPackage = trackedPackage;
	}

	public String getTrackingCity() {
		return trackingCity;
	}

	public void setTrackingCity(String trackingCity) {
		this.trackingCity = trackingCity;
	}

	public Date getTrackingTimestamp() {
		return trackingTimestamp;
	}

	public void setTrackingTimestamp(Date trackingTimestamp) {
		this.trackingTimestamp = trackingTimestamp;
	}

}
