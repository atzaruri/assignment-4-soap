package ro.lazarlaurentiu.sd.java.soap.server.dao;

import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import ro.lazarlaurentiu.sd.java.soap.server.models.Package;
import ro.lazarlaurentiu.sd.java.soap.server.models.User;

/**
 * Data Access Object for Packages.
 * 
 * @author Lazar Laurentiu
 */
public class PackagesDao {

	private static final Log LOGGER = LogFactory.getLog(PackagesDao.class);

	private SessionFactory factory;

	public PackagesDao(SessionFactory factory) {
		this.factory = factory;
	}

	public boolean createPackage(Package packageToCreate) {
		Session session = factory.openSession();
		Transaction transaction = null;

		try {
			transaction = session.beginTransaction();
			session.save(packageToCreate);
			transaction.commit();
		} catch (HibernateException e) {
			if (transaction != null) {
				transaction.rollback();
			}

			LOGGER.error("Method: createPackage()", e);

			return false;
		} finally {
			session.close();
		}

		return true;
	}

	@SuppressWarnings("unchecked")
	public Package getPackagesById(int packageId) {
		Session session = factory.openSession();
		Transaction transaction = null;
		List<Package> packages = null;

		try {
			transaction = session.beginTransaction();
			Query query = session.createQuery("FROM Package WHERE packageId = :packageId");
			query.setParameter("packageId", packageId);
			packages = query.list();
			transaction.commit();
		} catch (HibernateException e) {
			if (transaction != null) {
				transaction.rollback();
			}
			LOGGER.error("Method: getPackagesById()", e);
		} finally {
			session.close();
		}
		return packages != null && !packages.isEmpty() ? packages.get(0) : null;
	}

	@SuppressWarnings("unchecked")
	public List<Package> getPackagesForUser(User user) {
		Session session = factory.openSession();
		Transaction transaction = null;
		List<Package> packages = null;

		try {
			transaction = session.beginTransaction();
			Query query = session.createQuery("FROM Package WHERE sender = :sender or receiver = :receiver");
			query.setParameter("sender", user);
			query.setParameter("receiver", user);
			packages = query.list();
			transaction.commit();
		} catch (HibernateException e) {
			if (transaction != null) {
				transaction.rollback();
			}
			LOGGER.error("Method: getPackagesForUser()", e);
		} finally {
			session.close();
		}
		return packages;
	}

}